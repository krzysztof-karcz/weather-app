const http = require('http')
const path = require('path')

const express = require('express')
const app = express()

var webpack = require('webpack')
var webpackConfig = require(process.env.WEBPACK_CONFIG ? process.env.WEBPACK_CONFIG : '../../webpack.config')
var compiler = webpack(webpackConfig)

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true, publicPath: webpackConfig.output.publicPath
}))

app.get('*', (req, res) => res.sendFile(path.join(__dirname, '/index.html')))

const server = http.createServer(app)
server.listen(process.env.PORT || 5000, () => console.log('Listening on %j', server.address()))

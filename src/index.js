import { render } from 'react-dom'
import App from './pages/app'

// Load styles & icons

import './styles/main.scss'

render(
  <App />,
  document.getElementById('container')
)

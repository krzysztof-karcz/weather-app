import React from 'react'
import { Redirect } from 'react-router-dom' 

import cls from 'classnames'

// helpers
import { validateLogin } from '../helpers/validation'
import localStorage from '../helpers/localStorage'

class Login extends React.Component {
  constructor () {
    super()

    this.state = {
      chartsData: {},
      errored: false,
      errorMessage: null,
      loggedIn: localStorage.get('loggedIn')
    }

    this.onButtonClick = this.onButtonClick.bind(this)
  }

  onButtonClick (e) {
    e.preventDefault()
    const errors = validateLogin({ login: this.login, password: this.password })

    if (errors.length > 0) {
      this.setState({ errored: true, errorMessage: errors[0].message })
      return
    }

    localStorage.set('loggedIn', true)
    this.setState({ loggedIn: true })
  }

  render () {
    const { loggedIn, errorMessage } = this.state

    if (loggedIn) {
      return <Redirect to={'/charts'} />
    }

    const clsRoot = cls('weather-container')
    const clsForm = cls('weather-form', {
      'weather-form--errored': this.state.errored,
      'weather-form--hidden': this.state.loggedIn
    })

    return (
      <div className={clsRoot}>
        <div className={clsForm}>
          <h2>Login</h2>
          <form>
            <input name='login' type='text' key='login' ref={ref => (this.login = ref)} placeholder='login' autoFocus />
            <input name='password' type='password' key='password' ref={ref => (this.password = ref)} placeholder='password' />
            <button onClick={this.onButtonClick}>Get in!</button>
            <span className='form-errors'>{ errorMessage }</span>
          </form>
        </div>
      </div>
    )
  }
}

export default Login

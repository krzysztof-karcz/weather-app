import React from 'react'

const NotFound = () => (
  <div className='page-not-found'>Page cannot be found.</div>
)

export default NotFound

import React from 'react'

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Login from './login'
import Register from './register'
import Charts from './charts'
import NotFound from './notFound'

class App extends React.Component {
  render () {
    return (
      <Router>
        <Switch>
          <Route exact path='/' component={Register} />
          <Route exact path='/login' component={Login} />
          <Route exact path='/charts' component={Charts} />
          <Route exact path='*' component={NotFound} />
        </Switch>
      </Router>
    )
  }
}

export default App

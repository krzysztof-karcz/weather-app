import React from 'react'
import { Redirect } from 'react-router-dom'
import cls from 'classnames'

import localStorage from '../helpers/localStorage'

// components
import Loader from '../components/loader'
import BarChart from '../components/charts/barChart'
import LineChart from '../components/charts/lineChart'

class Charts extends React.Component {
  constructor () {
    super()

    this.state = {
      chartsData: null,
      loggedIn: localStorage.get('loggedIn')
    }

    this.onLoad = this.onLoad.bind(this)
    this.onLogoutClick = this.onLogoutClick.bind(this)
  }

  onLoad (data) {
    this.setState({ chartsData: data })
  }

  onLogoutClick () {
    localStorage.remove('loggedIn')
    this.setState({ loggedIn: false })
  }

  render () {
    const { chartsData, loggedIn } = this.state
    const clsRoot = cls('weather-container')

    if (!loggedIn) {
      return <Redirect to={'/login'} />
    }

    return (
      <div className={clsRoot}>
        <Loader data={chartsData} onLoad={this.onLoad} request={'getChartsData'}>
          <nav>
            <button onClick={this.onLogoutClick}>Logout</button>
          </nav>

          <div className='charts'>
            <BarChart data={chartsData} />
            <LineChart data={chartsData} />
          </div>
        </Loader>
      </div>
    )
  }
}

export default Charts

import React from 'react'
import { Link, Redirect } from 'react-router-dom'
import cls from 'classnames'

import { validateRegister } from '../helpers/validation'
import localStorage from '../helpers/localStorage'

class Register extends React.Component {
  constructor () {
    super()

    this.state = {
      created: localStorage.get('user'),
      loggedIn: localStorage.get('loggedIn'),
      errored: false,
      fields: {}
    }

    this.onInputChange = this.onInputChange.bind(this)
    this.onButtonClick = this.onButtonClick.bind(this)
    this.shouldDisable = this.shouldDisable.bind(this)
  }

  shouldDisable () {
    const { fields } = this.props
    const keys = Object.keys(fields)
    const filledCount = keys.filter(field => {
      return this.state.fields[field] && this.state.fields[field].length > 0
    }).length
    const fieldsCount = keys.length

    return filledCount !== fieldsCount
  }

  onButtonClick (e) {
    e.preventDefault()

    const isValid = validateRegister(this.state.fields)

    if (!isValid) {
      this.setState({ errorMessage: 'Passwords does not match.' })
      return
    }

    const { email, password } = this.state.fields

    localStorage.set('user', { email, password })
    this.setState({ created: true })
  }

  onInputChange (e) {
    const target = e.target
    const { name, value } = target

    this.setState(state => ({
      fields: {
        ...state.fields,
        [name]: value
      }
    }))
  }

  render () {
    const { fields } = this.props
    const { loggedIn, errorMessage } = this.state

    if (loggedIn) {
      return <Redirect to={'/charts'} />
    }

    const clsRoot = cls('weather-container')
    const clsForm = cls('weather-form', {
      'weather-form--errored': this.state.errored,
      'weather-form--hidden': this.state.created
    })

    const clsInfo = cls('register-info', {
      'register-info--visible': this.state.created
    })

    const fieldNames = Object.keys(fields)
    const formFields = fieldNames.map(field => {
      const isFirstField = fieldNames[0] === field
      const inputProps = {
        ...fields[field],
        onChange: this.onInputChange,
        name: field,
        key: field,
        autoFocus: isFirstField
      }

      return (
        <input {...inputProps} ref={ref => (this.name = ref)} />
      )
    })

    return (
      <div className={clsRoot}>
        <div className={clsForm}>
          <h2>Register</h2>
          <form>
            {formFields}
            <button onClick={this.onButtonClick} disabled={this.shouldDisable()}>Sign up!</button>
            <span className='form-errors'>{ errorMessage }</span>
          </form>
        </div>

        <div className={clsInfo}>
          <p>Account has been created.</p>
          <p>Use <strong>email</strong> as login and password you just provided to get in.</p>
          <p>
            <Link to={'/login'}>
              <button>Sign in</button>
            </Link>
          </p>
        </div>
      </div>
    )
  }
}

Register.defaultProps = {
  fields: {
    name: {
      placeholder: 'name',
      type: 'text'
    },
    lastname: {
      placeholder: 'lastname',
      type: 'text'
    },
    email: {
      placeholder: 'email',
      type: 'text'
    },
    phone: {
      placeholder: 'phone',
      type: 'text'
    },
    street: {
      placeholder: 'street',
      type: 'text'
    },
    city: {
      placeholder: 'city',
      type: 'text'
    },
    password: {
      placeholder: 'password',
      type: 'password'
    },
    confirmPassword: {
      placeholder: 'confirm password',
      type: 'password'
    }
  }
}

export default Register

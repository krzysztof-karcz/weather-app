const API_KEY = 'ce73f7b231ed24b57a9448e2ebaf9736'
const API_HOST = 'http://api.openweathermap.org'

const delay = (fn, timeout) => {
  return new Promise(resolve => {
    return setTimeout(() => resolve(fn()), timeout)
  })
}

const formatDate = date => {
  const month = date.getMonth() + 1
  const day = date.getDate()

  return `${day}/${month}`
}

const prepareSeries = list => {
  const series = {
    humidity: [],
    temperature: [],
    days: []
  }

  list.forEach((o, i) => {
    series.days.push(formatDate(new Date(o.dt_txt)))
    series.humidity.push(o.main.humidity)
    series.temperature.push(o.main.temp)
  })

  return series
}

let Requests = {}
Requests.getChartsData = async (timeout = 0) => {
  const url = `${API_HOST}/data/2.5/forecast?q=Krakow,pl&mode=json&appid=${API_KEY}&units=metric`

  try {
    const request = await window.fetch(url)
    const response = await request.json()

    const list = response.list

    if (!list) {
      return console.error('Data not loaded.')
    }

    return delay(prepareSeries.bind(null, list), timeout)
  } catch (err) {
    console.log('err', err)
  }
}

export default Requests

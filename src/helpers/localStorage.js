
let AppStorage = {}

function tryParse (data, defaultValue = data) {
  try {
    return JSON.parse(data)
  } catch (e) {
    return defaultValue
  }
}

AppStorage.get = key => {
  const storeValue = window.localStorage.getItem(key)
  return tryParse(storeValue)
}

AppStorage.set = (key, value) => {
  const storeValue = typeof value === 'object' ? JSON.stringify(value) : value
  window.localStorage.setItem(key, storeValue)
}

AppStorage.remove = key => {
  window.localStorage.removeItem(key)
}

export default AppStorage

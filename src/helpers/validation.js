import localStorage from './localStorage'

export const validateLogin = fields => {
  const user = localStorage.get('user')

  if (!user) {
    return [{ field: 'not-exist', message: 'User does not exist in our database' }]
  }

  const rules = {
    login: {
      value: user.email,
      message: 'Login incorrect'
    },
    password: {
      value: user.password,
      message: 'Password incorrect'
    }
  }

  let errors = []
  Object.keys(rules).forEach(field => {
    if (fields[field].value !== rules[field].value) {
      errors.push({ field, message: rules[field].message })
    }
  })

  return errors
}

export const validateRegister = fields => {
  const { password, confirmPassword } = fields

  return password === confirmPassword
}

import React from 'react'
import { Chart } from 'chart.js'

class LineChart extends React.Component {
  constructor () {
    super()

    this.draw = this.draw.bind(this)
  }

  draw () {
    const canvas = this.canvas
    if (!canvas) {
      return
    }

    const context = canvas.getContext('2d')
    const { data } = this.props
    const chart = new Chart(context, {
      type: 'line',
      data: {
        labels: data.days,
        datasets: [{
          label: 'Humidity',
          fill: false,
          borderColor: '#ff0000',
          pointBackgroundColor: '#ffffff',
          data: data.humidity
        }]
      },
      options: {}
    })
  }

  componentDidMount () {
    this.draw()
  }

  render () {
    return (
      <canvas id='lineChartCanvas' ref={ref => (this.canvas = ref)} />
    )
  }
}

export default LineChart

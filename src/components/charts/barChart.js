import React from 'react'
import { Chart } from 'chart.js'

class BarChart extends React.Component {
  constructor () {
    super()

    this.draw = this.draw.bind(this)
  }

  draw () {
    const canvas = this.canvas
    if (!canvas) {
      return
    }

    const context = canvas.getContext('2d')
    const { data } = this.props
    const chart = new Chart(context, {
      type: 'bar',
      data: {
        labels: data.days,
        datasets: [{
          label: 'Temperature',
          fill: false,
          backgroundColor: '#ffffff',
          borderColor: '#ff0000',
          borderWidth: 1,
          data: data.temperature
        }]
      },
      options: {}
    })
  }

  componentDidMount () {
    this.draw()
  }

  render () {
    return (
      <canvas id='barChartCanvas' ref={ref => (this.canvas = ref)} />
    )
  }
}

export default BarChart

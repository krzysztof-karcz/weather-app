import React from 'react'

// helpers
import Requests from '../helpers/request'

// defaults
const REQUESTS_DELAY = 2000

class Loader extends React.Component {
  async componentDidMount () {
    const { request } = this.props

    if (!Requests[request]) {
      return
    }

    const data = await Requests[this.props.request](REQUESTS_DELAY)
    this.props.onLoad(data)
  }

  render () {
    const { children, data } = this.props

    if (!data) {
      return (
        <div className='sk-circle'>
          <div className='sk-circle1 sk-child' />
          <div className='sk-circle2 sk-child' />
          <div className='sk-circle3 sk-child' />
          <div className='sk-circle4 sk-child' />
          <div className='sk-circle5 sk-child' />
          <div className='sk-circle6 sk-child' />
          <div className='sk-circle7 sk-child' />
          <div className='sk-circle8 sk-child' />
          <div className='sk-circle9 sk-child' />
          <div className='sk-circle10 sk-child' />
          <div className='sk-circle11 sk-child' />
          <div className='sk-circle12 sk-child' />
        </div>
      )
    }

    return <div className='loaded'>{ children }</div>
  }
}

export default Loader

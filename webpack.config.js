const ExtractTextPlugin = require('extract-text-webpack-plugin')

const plugins = [
  new ExtractTextPlugin({
    filename: 'styles.css',
    allChunks: true
  })
]

const extractCssLoader = ExtractTextPlugin.extract({
  fallback: 'style-loader',
  use: [
    { loader: 'css-loader', options: { importLoaders: 1 } },
    'postcss-loader',
    { loader: 'sass-loader' }
  ]
})

const rules = [
  {
    test: /\.(js|jsx)$/,
    exclude: /node_modules/,
    use: [
      'babel-loader'
    ]
  },
  {
    test: /\.(css|scss)$/,
    exclude: /node_modules/,
    use: [ 'css-hot-loader' ].concat(extractCssLoader)
  }
]

module.exports = {
  entry: './src/index.js',
  output: {
    path: __dirname,
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  module: {
    loaders: rules
  },
  plugins
}
